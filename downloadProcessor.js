var mcClient;
var util = require('util');

exports.processDownload = function(req, res){
	var query = req.url.substr(1).split("/");
    //var id = query[0];
   //var type = query[1];
	//var keyName = id + "_" + type;
	var keyName = query;
	mcClient.get(keyName, function(err, response) {
		if (!err) {
			//console.log(util.inspect(response));
			res.writeHead(200, {"Content-Type": "image/webp"});
			res.end(response[keyName].buffer);
			console.log("SERVED: " + keyName)			
		}else{
			res.writeHead(200, {"Content-Type": "text/plain"});
			res.end("not found" + keyName);
			console.log("404: " + keyName)
		}			
	});	
}

exports.init = function(client){
    mcClient = client;
}
