var mcService = require("./memcachedService");
var uploadProcessor = require("./uploadProcessor");
var downloadProcessor = require("./downloadProcessor");
var httpServer = require("./httpServer");

console.log("MAIN: Starting");

//console.log("MAIN: INIT LOGGER");

console.log("MAIN: MC START");
mcService.start(function(){
	uploadProcessor.init(mcService.client);
	downloadProcessor.init(mcService.client);


	httpServer.start(uploadProcessor, downloadProcessor);
});
