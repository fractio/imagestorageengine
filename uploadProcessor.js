var mcClient;
    fs = require('fs');

exports.processUpload = function (req, res){
    var query = req.url.substr(1).split("/");
    var id = query[0];
    var type = query[1];
    var buffer = new Buffer(0);

    req.on("data", function(data){
        buffer = Buffer.concat([buffer, data]);        
    });

    req.on("end", function(){
        console.log("ID: " + id);
        //console.log(util.inspect(buffer));
        
        var keyName = id + "_" + type;

        mcClient.set(keyName, buffer, { flags: 0, exptime: 0}, function(err, status) {
            if (!err) {
                res.writeHead(200, {'content-type': 'text/plain'});
                var msg = "ID: " + keyName + " STORED IN MEMCACHED";
                res.end(msg);
                console.log(msg);
                //fs.writeFileSync(keyName, buffer);
            }
        });
    });
}

exports.init = function(client){
    mcClient = client;
}
