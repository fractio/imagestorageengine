var http = require('http'),
    util = require('util')

var ip = process.env.OPENSHIFT_INTERNAL_IP || null,
    port = process.env.OPENSHIFT_INTERNAL_PORT || 8080;

var uploadProcessor,
	downloadProcessor;

var server = http.createServer(function (req, res) {
	if(req.method == "GET"){
		downloadProcessor.processDownload(req, res);
	}else{
		uploadProcessor.processUpload(req, res);
	}    
});

var start = function(uploadProcessorIncoming, downloadProcessorIncoming){	
	uploadProcessor = uploadProcessorIncoming;
	downloadProcessor = downloadProcessorIncoming;

	server.listen(port, ip);
	console.log('listening on ' + ip + ":" + port.toString());
}

exports.uploadProcessor = uploadProcessor;
exports.downloadProcessor = downloadProcessor;
exports.start = start;