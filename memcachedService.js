var mc = require('mc');

var client = 
	new mc.Client('pub-memcache-13365.us-east-1-3.1.ec2.garantiadata.com:13365',
 					mc.Adapter.raw);

function start(completeCallback){
	console.log("Connecting to mc")
	client.connect(function(){
		completeCallback();
	});
}

exports.start = start;
exports.client = client;